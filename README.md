# Automated Attendance System
## BS17-8-1
* Muhammad Mavlyutov - [Telegram](http://t.me/theMavl "@theMval")
* Vyacheslav Goreev - [Telegram](http://t.me/slavagoreev "@slavagoreev")
* Elvira Salikhova - [Telegram](http://t.me/elvirkavena "@elvirkavena")
* Artemii Bykov - [Telegram](http://t.me/Species_decipit "@Species_decipit")
## Useful Links
* Artifact 0 - Interview script: [Google Docs](https://docs.google.com/document/d/1iZLnSr4bX5vyPEqtAPuRwNOcLSNMkM2OOTcLgQ21q9Q/edit?usp=sharing)
* Artifact 1 - Interview analysis: [Google Docs](https://docs.google.com/document/d/1EX99iAtiU7MfDizSAub_lle_t75quo4mNsTBH9VmFkI/edit?usp=sharing)
* Artifact 2 - Tools Analysis: [Google Docs](https://docs.google.com/document/d/1pkU7FgxsADu4IzYLC3gdj0gxYgYhFuEaU_7wrLi9XCE/edit?usp=sharing)
* Artifact 3 - Impact mapping Reflection: [Google Docs](https://docs.google.com/document/d/1CNbGsaPVHbPTvTvm0-_cI99IO4L_2Kh4o8Rr8L3_cCA/edit?usp=sharing) [Impact Mapping](./pdf_files/Impact\ Mapping.pdf)
* Artifact 4 - Product Backlog + INVEST Criteria + User Stories: [Google Docs](https://docs.google.com/spreadsheets/d/1ZCG_cOuHZOGfYbfpDbZgek2lmvXpESGfBBX7QPgArBc/edit?usp=sharing) [Product Backlog](./pdf_files/Product\ Backlog.pdf)