from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('user/', include('user.urls')),
    path('education/', include('education.urls')),
    path('attendance/', include('attendance.urls')),
    path('statistic/', include('statistic.urls')),
    path('oauth/', include('auth.urls')),
]
