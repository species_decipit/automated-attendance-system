from django.urls import path
from . import views

urlpatterns = [
    path('course/', views.CourseCreateList.as_view()),
    path('course/<int:course_id>/', views.CourseRetrieveUpdateDestroy.as_view())
]