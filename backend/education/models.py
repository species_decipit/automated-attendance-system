from django.contrib.auth.models import User
from user.models import UserProfile
from django.db import models


class Course(models.Model):
    title = models.CharField(max_length=32)
    description = models.CharField(default='', max_length=512)
    participants = models.ManyToManyField(User, related_name='courses')

    @property
    def get_professors_list(self):
        return self.participants.filter(user_profile__privilege_level=UserProfile.PROFESSOR)

    @property
    def get_tas_list(self):
        return self.participants.filter(user_profile__privilege_level=UserProfile.TA)

    @property
    def get_students_list(self):
        return self.participants.filter(user_profile__privilege_level=UserProfile.STUDENT)

    def __str__(self):
        return '{0}. {1}'.format(self.id, self.title)
