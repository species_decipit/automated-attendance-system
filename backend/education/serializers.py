from rest_framework import serializers
from . import models
from user import models as users_models


class CourseSerializer(serializers.ModelSerializer):
    professors = serializers.SerializerMethodField(read_only=True)
    students = serializers.SerializerMethodField(read_only=True)
    tas = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_professors(obj):
        return obj.get_professors_list.values('id', 'first_name', 'last_name', 'email')

    def get_students(self, obj):
        if self.context.get('request').user.user_profile.privilege_level > users_models.UserProfile.STUDENT:
            return obj.get_students_list.values('id', 'first_name', 'last_name', 'email')
        else:
            return []

    @staticmethod
    def get_tas(obj):
        return obj.get_tas_list.values('id', 'first_name', 'last_name', 'email')

    class Meta:
        model = models.Course
        fields = ('id', 'title', 'description', 'professors', 'students', 'tas')
