from rest_framework.generics import get_object_or_404
from user import models as user_models
from rest_framework import generics
from . import serializers
from . import models


class CourseCreateList(generics.ListCreateAPIView):
    http_method_names = ('get',)
    serializer_class = serializers.CourseSerializer

    def get_queryset(self):
        user_profile = self.request.user.user_profile

        if user_profile.privilege_level == user_models.UserProfile.DOE:
            return models.Course.objects.all()
        else:
            return user_profile.get_courses_list


class CourseRetrieveUpdateDestroy(generics.RetrieveDestroyAPIView):
    http_method_names = ('get',)
    serializer_class = serializers.CourseSerializer
    queryset = models.Course.objects.all()

    def get_object(self):
        return get_object_or_404(self.get_queryset(), pk=self.kwargs['course_id'])
