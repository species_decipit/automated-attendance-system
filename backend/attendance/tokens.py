from django.utils.timezone import now
from django.conf import settings
from . import models
import binascii
import hashlib
import base64
import json

HASH_TYPE = 'sha256'
NUMBER_OF_ROUNDS = 100000


def is_valid_signature(base64_payload: str, base64_signature: str) -> bool:
    try:
        return base64.urlsafe_b64decode(base64_signature) == create_signature(bytes(base64_payload, 'utf-8'))
    except binascii.Error as e:
        return False


def get_payload(token: str) -> dict:
    return json.loads(base64.urlsafe_b64decode(token.split('.')[0]))


def is_not_expired(base64_payload: str) -> bool:
    return json.loads(base64.urlsafe_b64decode(base64_payload))['exp_time'] >= now().timestamp()


def create_signature(base64_payload: bytes) -> bytes:
    return binascii.hexlify(hashlib.pbkdf2_hmac(HASH_TYPE, base64_payload,
                                                bytes(settings.SECRET_KEY, 'utf-8'), NUMBER_OF_ROUNDS))


def is_valid(token: str) -> bool:
    try:
        base64_payload, base64_signature = token.split('.')
    except ValueError as e:
        return False
    return is_valid_signature(base64_payload, base64_signature) and is_not_expired(base64_payload)


def create_token(event: models.Event) -> str:
    payload = {'event_id': event.id,
               'exp_time': now().timestamp() + event.token_expiration}
    base64_payload = base64.urlsafe_b64encode(json.dumps(payload).encode())
    signature = create_signature(base64_payload)
    return base64_payload + b'.' + base64.urlsafe_b64encode(signature)
