from django.urls import path
from . import views

urlpatterns = [
    path('event/', views.EventCreateList.as_view()),
    path('event/<int:pk>/', views.EventRetrievePatchDestroy.as_view()),
    path('check-in-url/<uuid:uuid_event>/', views.get_check_in_url),
    path('check-in/<str:token>/', views.check_in),
    path('event-types/', views.get_types_of_event),
    path('event/manual/', views.manual),
]
