from django.contrib.auth import models as auth_models
from rest_framework import serializers
from user import models as user_models
from . import models


class EventSerializer(serializers.ModelSerializer):
    start_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    end_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    instructor = serializers.IntegerField(source='instructor.id', read_only=True)
    lesson_type = serializers.ChoiceField(choices=models.Event.LESSON_TYPE, required=True)
    token_expiration = serializers.IntegerField(required=True)
    title = serializers.CharField(required=True)

    def create(self, validated_data):
        request = self.context.get('request')
        if validated_data[
            'course'
        ].id not in request.user.user_profile.get_courses_list.values_list('id', flat=True):
            raise serializers.ValidationError('You are not enrolled in this course')

        event_instance = models.Event.objects.create(
            **{
                **validated_data,
                'instructor': request.user,
                'lesson_type': validated_data['lesson_type'],
            }
        )

        return event_instance

    class Meta:
        model = models.Event
        fields = '__all__'
        read_only_fields = ('id', 'uuid', 'instructor')


class ManualSerializer(serializers.Serializer):
    CHECK_OUT = 0
    CHECK_IN = 1
    ACTION = ((CHECK_OUT, 'CHECK_OUT'), (CHECK_IN, 'CHECK_IN'))

    student = serializers.PrimaryKeyRelatedField(queryset=auth_models.User.objects.all(), required=True)
    event = serializers.PrimaryKeyRelatedField(queryset=models.Event.objects.all(), required=True)
    action = serializers.ChoiceField(choices=ACTION, required=True)

    @staticmethod
    def validate_student(value):
        if value.user_profile.privilege_level != user_models.UserProfile.STUDENT:
            raise serializers.ValidationError('Such user is not a student')
        return value

    def validate_event(self, value):
        request = self.context.get('request')
        if value not in request.user.user_profile.get_events:
            raise serializers.ValidationError('You are not enrolled to this course')
        return value
