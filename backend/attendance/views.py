from django.utils.translation import gettext as _
from user import models as user_model
from rest_framework import decorators
from rest_framework import generics
from rest_framework import response
from rest_framework import status
from . import permissions
from . import serializers
from . import models
from . import tokens
import permissions as global_permissions


class EventCreateList(generics.ListCreateAPIView):
    http_method_names = ('get', 'post')
    serializer_class = serializers.EventSerializer
    permission_classes = (permissions.EventCreateListPermissions,)

    def get_queryset(self):
        user_profile = self.request.user.user_profile

        if user_profile.privilege_level == user_model.UserProfile.DOE:
            return models.Event.objects.all()
        else:
            return models.Event.objects.filter(
                course__in=user_profile.get_courses_list.values_list('id', flat=True)
            )


class EventRetrievePatchDestroy(generics.RetrieveUpdateDestroyAPIView):
    http_method_names = ('patch', 'get', 'delete')
    serializer_class = serializers.EventSerializer
    queryset = models.Event.objects.all()
    permission_classes = (permissions.EventRetrieveUpdateDestroyPermissions,)


@decorators.api_view(['GET'])
def get_types_of_event(request):
    result = [{'type_id': item[0], 'type_name': item[1]} for item in models.Event.LESSON_TYPE]
    return response.Response(data=result, status=status.HTTP_200_OK)


@decorators.api_view(['GET'])
@decorators.permission_classes([global_permissions.IsTA | global_permissions.IsProfessor])
def get_check_in_url(request, uuid_event):
    from urllib.parse import urlsplit

    try:
        event_instance = models.Event.objects.get(uuid=uuid_event)
    except models.Event.DoesNotExist:
        return response.Response(
            data={'detail': _('Such event does not exist')}, status=status.HTTP_404_NOT_FOUND
        )
    if event_instance.instructor != request.user:
        return response.Response(
            data={'detail': _('You do not own this event')}, status=status.HTTP_400_BAD_REQUEST
        )
    scheme, domain, *others = urlsplit(request.build_absolute_uri(None))
    signed_token = tokens.create_token(event_instance).decode('utf-8')
    return response.Response(
        data={
            'check_in_url': '{0}://{1}/attendance/check-in/{2}/'.format(
                scheme, domain, signed_token
            ),
            'number_of_checked_in': models.Attendance.objects.filter(event=event_instance).count(),
        },
        status=status.HTTP_200_OK,
    )


@decorators.api_view(['GET'])
def check_in(request, token):
    if not tokens.is_valid(token):
        return response.Response(
            data={'detail': _('Invalid token')}, status=status.HTTP_400_BAD_REQUEST
        )

    payload = tokens.get_payload(token)
    try:
        event_instance = models.Event.objects.get(pk=payload['event_id'])
    except models.Event.DoesNotExist:
        return response.Response(
            data={'detail': _('Ooooops... Such event does not exist. Write to administrator!!!')},
            status=status.HTTP_404_NOT_FOUND,
        )
    if models.Attendance.objects.filter(event=event_instance, student=request.user):
        return response.Response(
            data={'detail': _('You are already checked in!!!')}, status=status.HTTP_403_FORBIDDEN
        )

    models.Attendance.objects.create(student=request.user, event=event_instance)

    return response.Response(
        data={'detail': _('You are successfully check in')}, status=status.HTTP_200_OK
    )


@decorators.api_view(['POST'])
@decorators.permission_classes([global_permissions.IsTA | global_permissions.IsProfessor])
def manual(request):
    serializer = serializers.ManualSerializer(data=request.data, context={'request': request})
    if not serializer.is_valid():
        return response.Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    if serializer.data['action'] == serializers.ManualSerializer.CHECK_IN:
        models.Attendance.objects.get_or_create(
            student=serializer.validated_data['student'], event=serializer.validated_data['event']
        )
    else:
        models.Attendance.objects.filter(
            student=serializer.validated_data['student'], event=serializer.validated_data['event']
        ).delete()

    return response.Response(status=status.HTTP_200_OK)
