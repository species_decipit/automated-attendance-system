from rest_framework import permissions
from user import models as user_models


class EventCreateListPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        return (request.method in permissions.SAFE_METHODS) or (
            request.user
            and request.user.is_authenticated
            and (
                request.user.user_profile.privilege_level
                in [user_models.UserProfile.TA, user_models.UserProfile.PROFESSOR]
            )
        )


class EventRetrieveUpdateDestroyPermissions(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return (
            request.user and request.user.is_authenticated and obj.instructor == request.user
        ) or (request.user.user_profile.privilege_level == user_models.UserProfile.DOE)
