from django.contrib.auth.models import User
from education.models import Course
from django.db import models
import uuid


class Event(models.Model):
    LESSON_TYPE = ((0, 'LECTURE'),
                   (1, 'TUTORIAL'),
                   (2, 'LAB'))

    instructor = models.ForeignKey(User, on_delete=models.PROTECT, related_name='events',
                                   limit_choices_to={'user_profile__privilege_level__gte': 2})
    course = models.ForeignKey(Course, on_delete=models.PROTECT, related_name='events')
    title = models.CharField(max_length=128)
    lesson_type = models.IntegerField(choices=LESSON_TYPE, default=0)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    meta = models.TextField(max_length=1024, default='')
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    token_expiration = models.IntegerField(default=10)

    @property
    def get_lesson_type(self):
        return self.LESSON_TYPE[self.lesson_type][1]

    def __str__(self):
        return '{0}. {1} - {2}, {3}'.format(self.id, self.course.title, self.get_lesson_type,
                                            self.start_time.strftime('%Y-%m-%d'))


class Attendance(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='attendance')
    student = models.ForeignKey(User, on_delete=models.CASCADE, related_name='attendance',
                                limit_choices_to={'user_profile__privilege_level': 0})

    def __str__(self):
        return '{0} - {1}'.format(self.event, self.student.user_profile.get_full_name)
