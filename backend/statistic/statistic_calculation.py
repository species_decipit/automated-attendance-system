from education import serializers as education_serializers
from attendance import serializers as attendance_serializers
from attendance import models as attendance_models
from education import models as education_models
from user import serializers as user_serializers
from django.contrib.auth.models import User
from rest_framework.request import Request


def event_statistic(request: Request, event: attendance_models.Event) -> dict:
    res = {'event': attendance_serializers.EventSerializer(event).data}
    ls = []
    qs = attendance_models.Attendance.objects.filter(event=event)
    for student in event.course.get_students_list:
        ls.append({'student': user_serializers.UserDetailForEventSerializer(student, context={'request': request}).data,
                   'visited': bool(qs.filter(event=event, student=student))})
    res['attendance'] = ls
    res['total_visited'] = qs.count()
    return res


def personal_statistic(request: Request, student: User, from_date: str, to_date: str, course_ids: list) -> list:
    res = []
    for sci in course_ids:
        qs = attendance_models.Event.objects.filter(course=sci)
        if from_date and to_date:
            qs = qs.filter(start_time__range=(from_date + 'T00:00', to_date + 'T23:59'))
        course_events = attendance_serializers.EventSerializer(qs, many=True).data
        course_info = education_serializers.CourseSerializer(education_models.Course.objects.get(pk=sci),
                                                             context={'request': request}).data
        res.append({'course': course_info,
                    'events': [{'event': event,
                                'visited': bool(attendance_models.Attendance.objects.filter(student=student,
                                                                                            event=event['id']))}
                               for event in course_events]})

    return res
