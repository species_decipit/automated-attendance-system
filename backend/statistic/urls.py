from django.urls import path
from . import views

urlpatterns = [
    path('course_statistic/<int:course_id>/', views.course_statistic),
    path('personal_statistic/<int:student_id>/', views.personal_statistic),
    path('own_statistic/', views.own_total_statistic),
    path('own_statistic/<int:course_id>/', views.own_course_statistic),
    path('event_statistic/<int:event_id>/', views.event_statistic),
]
