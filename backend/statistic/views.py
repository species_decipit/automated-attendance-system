from rest_framework.decorators import api_view, permission_classes
from attendance import serializers as attendance_serializers
from education import serializers as education_serializers
from attendance import models as attendance_models
from education import models as education_models
from rest_framework.response import Response
from django.contrib.auth.models import User
from user import models as user_models
from . import statistic_calculation
from rest_framework import status
from django.db import models
import permissions


@api_view(['GET'])
@permission_classes([permissions.IsProfessor | permissions.IsDOE])
def event_statistic(request, event_id):
    try:
        event_instance = attendance_models.Event.objects.get(pk=event_id)
    except attendance_models.Event.DoesNotExist:
        return Response(data={'detail': 'Such event does not exist'}, status=status.HTTP_404_NOT_FOUND)
    res = statistic_calculation.event_statistic(request, event_instance)

    return Response(data=res, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([permissions.IsProfessor | permissions.IsDOE])
def course_statistic(request, course_id):
    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)

    qs = attendance_models.Attendance.objects.filter(event__course=course_id)
    if from_date and to_date:
        qs = qs.filter(event__start_time__range=(from_date + 'T00:00', to_date + 'T23:59'))
    if not qs:
        return Response(data={'detail': 'There are no statistic for such course'}, status=status.HTTP_404_NOT_FOUND)

    qs = qs.values('event').annotate(total=models.Count('event')).order_by('total')
    ls = []
    for i in range(len(qs)):
        ls.append(attendance_serializers.EventSerializer(
            attendance_models.Event.objects.get(pk=qs[i]['event'])).data)
        ls[-1]['total'] = qs[i]['total']

    ls = sorted(ls, key=lambda k: k['start_time'])
    result = {'course': education_serializers.CourseSerializer(education_models.Course.objects.get(pk=course_id),
                                                               context={'request': request}).data, 'events': ls}

    return Response(result, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([permissions.IsProfessor | permissions.IsDOE])
def personal_statistic(request, student_id):
    try:
        student = User.objects.get(pk=student_id)
    except User.DoesNotExist:
        return Response(data={'detail': 'No such student'}, status=status.HTTP_404_NOT_FOUND)
    else:
        if student.user_profile.privilege_level != user_models.UserProfile.STUDENT:
            return Response(data={'detail': 'Such user is not a student'}, status=status.HTTP_400_BAD_REQUEST)

    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)
    courses_id = student.user_profile.get_courses_list.values_list('id', flat=True)
    res = statistic_calculation.personal_statistic(request, student, from_date, to_date, courses_id)
    for item in res:
        item['course']['students'] = []
    return Response(data=res, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([permissions.IsStudent])
def own_total_statistic(request):
    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)
    student = request.user
    courses_id = student.user_profile.get_courses_list.values_list('id', flat=True)

    return Response(data=statistic_calculation.personal_statistic(request, student, from_date, to_date, courses_id),
                    status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes([permissions.IsStudent])
def own_course_statistic(request, course_id):
    try:
        education_models.Course.objects.get(pk=course_id)
    except education_models.Course.DoesNotExist:
        return Response(data={'detail': 'Such course does not exist'}, status=status.HTTP_400_BAD_REQUEST)

    from_date = request.GET.get('from_date', None)
    to_date = request.GET.get('to_date', None)

    res = statistic_calculation.personal_statistic(request, request.user, from_date, to_date, [course_id])[0]
    return Response(data=res, status=status.HTTP_200_OK)
