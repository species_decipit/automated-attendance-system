from rest_framework.permissions import BasePermission
from user import models as user_models


class IsDOE(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and request.user.user_profile.privilege_level == \
               user_models.UserProfile.DOE


class IsProfessor(BasePermission):
    def has_permission(self, request, view):
        course_id = view.kwargs.get('course_id', None)
        event_id = view.kwargs.get('event_id', None)
        if course_id and course_id not in request.user.user_profile.get_courses_list.values_list('id', flat=True):
            return False
        if event_id and event_id not in  request.user.user_profile.get_events.values_list('id', flat=True):
            return False
        return request.user and request.user.is_authenticated and request.user.user_profile.privilege_level == \
               user_models.UserProfile.PROFESSOR


class IsTA(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and request.user.user_profile.privilege_level == \
               user_models.UserProfile.TA


class IsStudent(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and request.user.user_profile.privilege_level == \
               user_models.UserProfile.STUDENT
