from rest_auth import serializers as rest_auth_serializers
from rest_framework import serializers
from education import serializers as education_serializers
from . import models


class UserProfileDetailSerializer(serializers.ModelSerializer):
    privilege_level = serializers.SerializerMethodField()

    @staticmethod
    def get_privilege_level(obj):
        return obj.get_privilege_level

    class Meta:
        model = models.UserProfile
        fields = ('privilege_level',)


class UserDetailsSerializer(rest_auth_serializers.UserDetailsSerializer):
    user_profile = UserProfileDetailSerializer()
    courses = serializers.SerializerMethodField()

    def get_courses(self, obj):
        courses = education_serializers.CourseSerializer(obj.courses, many=True,
                                                         context={'request': self.context.get('request')}).data
        for course in courses:
            course['students'] = []
        return courses

    class Meta:
        fields = rest_auth_serializers.UserDetailsSerializer.Meta.fields + ('user_profile', 'courses')
        model = rest_auth_serializers.UserDetailsSerializer.Meta.model


class UserDetailForEventSerializer(UserDetailsSerializer):
    def get_courses(self, obj):
        return []
