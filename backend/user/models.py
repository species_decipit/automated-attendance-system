from django.contrib.auth.models import User
from django.db import models


class UserProfile(models.Model):
    STUDENT = 0
    TA = 1
    PROFESSOR = 2
    DOE = 3

    PRIVILEGE_LEVEL = ((STUDENT, 'STUDENT'),
                       (TA, 'TA'),
                       (PROFESSOR, 'PROFESSOR'),
                       (DOE, 'DOE'))

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True,
                                related_name='user_profile')
    privilege_level = models.IntegerField(choices=PRIVILEGE_LEVEL, default=0)

    @property
    def get_privilege_level(self):
        return self.PRIVILEGE_LEVEL[self.privilege_level][1]

    @property
    def get_courses_list(self):
        return self.user.courses.all()

    @property
    def get_events(self):
        return self.user.events.all()

    @property
    def get_full_name(self):
        return '{0} {1}'.format(self.user.first_name, self.user.last_name)

    def __str__(self):
        return '{0} {1}'.format(self.user.first_name, self.user.last_name)
