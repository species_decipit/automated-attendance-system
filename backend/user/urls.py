from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import verify_jwt_token
from rest_auth import views as rest_auth_views
from . import views
from django.urls import path

urlpatterns = [
    path('login/', obtain_jwt_token),
    path('token-refresh/', refresh_jwt_token),
    path('token-verify/', verify_jwt_token),
    path('profile/', rest_auth_views.UserDetailsView.as_view()),
    path('profile/<int:user_id>/', views.UserList.as_view()),
    path('password/change/', rest_auth_views.PasswordChangeView.as_view()),
]
