from rest_framework.generics import get_object_or_404
from django.contrib.auth import models
from rest_framework import generics
from . import serializers
import permissions


class UserList(generics.RetrieveAPIView):
    serializer_class = serializers.UserDetailsSerializer
    permission_classes = ((permissions.IsTA | permissions.IsProfessor | permissions.IsDOE),)
    queryset = models.User.objects.all()

    def get_object(self):
        return get_object_or_404(self.get_queryset(), pk=self.kwargs['user_id'])
