from rest_framework import decorators
from rest_framework import response
from rest_framework import status


@decorators.api_view(['POST'])
@decorators.permission_classes([])
@decorators.authentication_classes([])
def sso_auth(request):
    open('logs.txt', 'w').write(str(request.data))
    return response.Response(data=request.data, status=status.HTTP_200_OK)
