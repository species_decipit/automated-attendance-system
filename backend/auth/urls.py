from . import views
from django.urls import path

urlpatterns = [
    path('callback/', views.sso_auth),
]
