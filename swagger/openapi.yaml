openapi: 3.0.0
info:
  title: Automated Attendance System
  version: '1.0'
tags:
  - name: Users
    description: Everything about Users
  - name: Education
    description: Everything about Courses
  - name: Attendance
    description: Everything about Attendance
  - name: Personal Statistic
    description: Everything about Personal Statistic
  - name: Course Statistic
    description: Everything about Course Statistic
paths:
  /user/login/:
    post:
      tags:
        - Users
      summary: User login
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                username:
                  type: string
                password:
                  type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/JWTToken'
  /user/profile/:
    get:
      tags:
        - Users
      summary: Get OWN userprofile. Course field 'students' will be empty
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
  '/user/profile/{user_id}/':
    get:
      tags:
        - Users
      parameters:
        - in: path
          name: user_id
          required: true
          description: User ID
          schema:
            type: integer
      summary: Someone else's profile. Course field 'students' will be empty
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
  /user/password/change/:
    post:
      tags:
        - Users
      summary: Change user password
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                old_password:
                  type: string
                new_password1:
                  type: string
                new_password2:
                  type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  detail:
                    type: string
  /user/token-refresh/:
    post:
      tags:
        - Users
      summary: Refresh JWT
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                token:
                  type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/JWTToken'
  /education/course/:
    get:
      tags:
        - Education
      summary: Get list of (own) courses
      description: >-
        If you are DoE staff, you will get ALL courses, otherwise (i.e. student,
        TA, professor) you will get your enrolled courses. Moreover, student field will be empty if user is student
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Course'
  '/education/course/{course_id}/':
    get:
      tags:
        - Education
      parameters:
        - in: path
          name: course_id
          required: true
          description: Course ID
          schema:
            type: integer
      summary: Get info about particular course
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Course'
  /attendance/event/:
    get:
      tags:
        - Attendance
      summary: Get list of (own) events
      description: >-
        If you are DoE staff, you will get ALL events, otherwise (i.e. student,
        TA, professor) you will get events related to your enrolled courses
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Event'
    post:
      tags:
        - Attendance
      summary: Create new event
      requestBody:
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                start_time:
                  type: string
                end_time:
                  type: string
                course:
                  type: integer
                lesson_type:
                  type: integer
                meta:
                  type: string
                token_expiration:
                  type: integer
                title:
                  type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Event'
  /attendance/event/{event_id}/:
    get:
      tags:
        - Attendance
      summary: Retrieve event by ID
      parameters:
        - in: path
          name: event_id
          description: Event ID
          schema:
            type: integer
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Event'
    patch:
      tags:
        - Attendance
      summary: Update event by ID
      parameters:
        - in: path
          name: event_id
          description: Event ID
          schema:
            type: integer
      requestBody:
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                start_time:
                  type: string
                end_time:
                  type: string
                lesson_type:
                  type: integer
                token_expiration:
                  type: string
                title:
                  type: string
                meta:
                  type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Event'
    delete:
      tags:
        - Attendance
      summary: Destroy event by ID
      parameters:
        - in: path
          name: event_id
          description: Event ID
          schema:
            type: integer
      responses:
        204:
          description: No Content
  /attendance/check-in-url/{uuid_event}/:
    get:
      tags:
        - Attendance
      summary: Return Check-IN URL for the event
      parameters:
        - in: path
          description: UUID of the Event
          required: true
          name: uuid_event
          schema:
            type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CheckINUrl'
  /attendance/check-in/{token}/:
    get:
      tags:
        - Attendance
      summary: Check IN on the event
      parameters:
        - in: path
          description: Token
          required: true
          name: token
          schema:
            type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Detail'
              example:
                detail: You are successfully check in
        400:
          description: BAD REQUEST
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Detail'
              example:
                datail: Invalid token
        403:
          description: FORBIDDEN
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Detail'
              example:
                detail: You are already checked in!!!
        404:
          description: NOT FOUND
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Detail'
              example:
                detail: Ooooops... Such event does not exist. Write to administrator!!!
  /attendance/event-types/:
    get:
      tags:
        - Attendance
      summary: Return list of event types
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/EventType'
  /attendance/event/manual/:
    post:
      tags:
        - Attendance
      summary: Check-in/Check-out manually
      requestBody:
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                student:
                  type: integer
                event:
                  type: integer
                action:
                  description: 0 - Check-in, 1 - Check-out
                  type: integer
      responses:
        200:
          description: OK
  /statistic/event_statistic/{event_id}/:
    get:
      tags:
        - Course Statistic
      summary: Statstic for particular event
      parameters:
        - in: path
          name: event_id
          description: Event ID
          schema:
            type: integer
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  event:
                    $ref: '#/components/schemas/Event'
                  attendance:
                    type: array
                    items:
                      type: object
                      properties:
                        student:
                          $ref: '#/components/schemas/User'
                        visited:
                          type: boolean
                  total_visited:
                    type: integer
  '/statistic/personal_statistic/{student_id}/':
    get:
      tags:
        - Personal Statistic
      summary: Personal statistic for ALL enroled student`s courses. Course field 'studens' will be empty for performance
      parameters:
        - in: path
          name: student_id
          schema:
            type: integer
          required: true
          description: Student ID
        - in: query
          name: from_date
          description: The start of date interval
          schema:
            type: string
        - in: query
          name: to_date
          description: The end of date interval
          schema:
            type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    course:
                      $ref: '#/components/schemas/Course'
                    events:
                      type: array
                      items:
                        type: object
                        properties:
                          event:
                            $ref: '#/components/schemas/Event'
                          visited:
                            type: boolean
  '/statistic/course_statistic/{course_id}/':
    get:
      tags:
        - Course Statistic
      summary: Statistic for all course`s events in particular period
      parameters:
        - in: path
          name: course_id
          required: true
          description: Course ID
          schema:
            type: integer
        - in: query
          name: from_date
          description: The start of date interval
          schema:
            type: string
        - in: query
          name: to_date
          description: The end of date interval
          schema:
            type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  course:
                    $ref: '#/components/schemas/Course'
                  events:
                    type: array
                    items:
                      type: object
                      properties:
                        id:
                          type: integer
                        start_time:
                          type: string
                        end_time:
                          type: string
                        instructor:
                          type: integer
                        lesson_type:
                          type: integer
                        course:
                          type: integer
                        total:
                          type: integer
  /statistic/own_statistic/:
    get:
      tags:
        - Personal Statistic
      parameters:
        - in: query
          name: from_date
          description: The start of date interval
          schema:
            type: string
        - in: query
          name: to_date
          description: The end of date interval
          schema:
            type: string
      summary: Get OWN statistic for all enrolled courses. Course field 'students' will be empty
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    course:
                      $ref: '#/components/schemas/Course'
                    events:
                      type: array
                      items:
                        type: object
                        properties:
                          event:
                            $ref: '#/components/schemas/Event'
                          visited:
                            type: boolean
  /statistic/own_statistic/{course_id}/:
    get:
      tags:
        - Personal Statistic
      summary: Get OWN statistic for particular course. Course field 'students' will be empty
      parameters:
        - in: path
          name: course_id
          required: true
          description: Course ID
          schema:
            type: integer
        - in: query
          name: from_date
          description: The start of date interval
          schema:
            type: string
        - in: query
          name: to_date
          description: The end of date interval
          schema:
            type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  course:
                    $ref: '#/components/schemas/Course'
                  events:
                    type: array
                    items:
                      type: object
                      properties:
                        event:
                          $ref: '#/components/schemas/Event'
                        visited:
                          type: boolean
components:
  securitySchemes:
    JWTAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
  schemas:
    BriefUser:
      properties:
        id:
          type: integer
        first_name:
          type: string
        last_name:
          type: string
        email:
          type: string
    User:
      properties:
        pk:
          type: integer
        username:
          type: string
        email:
          type: string
        first_name:
          type: string
        last_name:
          type: string
        user_profiles:
          type: object
          properties:
            privilege_level:
              type: string
        courses:
          type: array
          items:
            $ref: '#/components/schemas/Course'
    Event:
      properties:
        id:
          type: integer
        title:
          type: string
        start_time:
          type: string
        end_time:
          type: string
        instructor:
          type: integer
        lesson_type:
          type: integer
        course:
          type: integer
        meta:
          type: string
        uuid:
          type: string
        token_expiration:
          type: integer
    Course:
      properties:
        id:
          type: integer
        title:
          type: string
        description:
          type: string
        professors:
          type: array
          items:
            $ref: '#/components/schemas/BriefUser'
        students:
          type: array
          items:
            $ref: '#/components/schemas/BriefUser'
        tas:
          type: array
          items:
            $ref: '#/components/schemas/BriefUser'
    EventType:
      properties:
        type_id:
          type: integer
        type_name:
          type: string
    CheckINUrl:
      properties:
        check_in_url:
          type: string
    Detail:
      properties:
        detail:
          type: string
    JWTToken:
      properties:
        token:
          type: string
      example:
        token: >-
          eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6InRoZXVuc2lnbmVkZmFpcm9uIiwiZXhwIjoxNTQ1NzY3NzM5LCJlbWFpbCI6InRoZXVuc2lnbmVkZmFpcm9uQGdtYWlsLmNvbSIsIm9yaWdfaWF0IjoxNTQ1NzY3NDM5fQ.W7HgZZ8CSm0BKV0d96V-fwUIWkCERK8cq_CiNQlE0ew
